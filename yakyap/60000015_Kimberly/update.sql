SELECT * FROM t_chat_role_setting where role_id = '60000015';

insert into t_chat_role_setting (role_id, role_name, role_system, model_name, role_tags,
role_icon, role_image, role_avatars, role_animations, score, status, is_new, is_vip, is_nsfw, params)
VALUES ('60000015', 'Kimberly', 'You are my love', 'mythalion',
'["Beauty", "Fashion", "Dancing", "Pop Music"]',
'https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_avatar.png',
'https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly.jpg',
'[]', '[]',
876, 1, 1, 0, 0, '{}');

insert into t_chat_pkg_role (role_id, pkg, start_cv, end_cv, weight, status)
VALUES ('60000015', 'com.yakyap.ai.chat', '0.0.01', '9.9.99', 1, 1);

UPDATE t_chat_role_setting SET interests = '["20000001"]' WHERE role_id = '60000015' AND role_name = 'Kimberly';
UPDATE t_chat_role_setting SET params = '{}' WHERE role_id = '60000015' AND role_name = 'Kimberly';
UPDATE t_chat_role_setting SET gender = 2 WHERE role_id = '60000015' AND role_name = 'Kimberly';
UPDATE t_chat_role_setting SET is_nsfw = 0 WHERE role_id = '60000015' AND role_name = 'Kimberly';

UPDATE t_chat_role_setting SET role_icon = 'https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_avatar.png'
WHERE role_id = '60000015' AND role_name = 'Kimberly';

UPDATE t_chat_role_setting SET role_image = 'https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly.jpg'
WHERE role_id = '60000015' AND role_name = 'Kimberly';


UPDATE t_chat_role_setting SET role_avatars = '["https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_1.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_2.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_3.jpg"]'
WHERE role_id = '60000015' AND role_name = 'Kimberly';

-- UPDATE t_chat_role_setting SET role_animations = '["https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_1.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_2.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_3.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/yakyap_roles/Kimberly_4.mp4"]'
-- WHERE role_id = '60000015' AND role_name = 'Kimberly';

select t1.role_id, t1.role_name, count(t2.asset_id) as c
from t_chat_role_setting t1 left join t_chat_role_media_asset t2
on t1.role_id = t2.role_id
where t1.role_id like '30%'
and t1.role_id % 2 = 1
group by 1, 2 order by c;

select t1.role_id, t1.role_name, count(t2.asset_id) as c
from t_chat_role_setting t1 left join t_chat_role_media_asset t2
on t1.role_id = t2.role_id
where t1.role_id like '40%'
group by 1, 2 order by c;

