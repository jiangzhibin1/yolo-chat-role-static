SELECT * FROM t_chat_role_setting where role_id = '40000010';

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.and.whisper', 'Android', '0.0.01', '9.9.99', '40000010', 'Yor Forger',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Yor Forger_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Yor Forger.jpg',
'[]', '[]',
1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.ios.whisperai', 'iOS', '0.0.01', '9.9.99', '40000010', 'Yor Forger',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Yor Forger_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Yor Forger.jpg',
'[]', '[]',
1, 1);


UPDATE t_chat_role set role_name = 'Yor Forger' WHERE role_id like '40%' AND role_name = 'Yorfuj';

UPDATE t_chat_role SET interests = '["20000003"]' WHERE role_id like '40%' AND role_name = 'Yor Forger';
UPDATE t_chat_role SET gender = 2 WHERE role_id like '40%' AND role_name = 'Yor Forger';
UPDATE t_chat_role SET role_avatars = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_1.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj__2.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_3.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_4.jpg"]'
WHERE role_id like '40%' AND role_name = 'Yor Forger';

UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_1.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_2.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_3.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_4.mp4"]'
WHERE role_id like '40%' AND role_name = 'Yor Forger';
UPDATE t_chat_role_setting SET role_animations = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_1.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_2.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_3.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Yorfuj_4.mp4"]'
WHERE role_id like '40%' AND role_name = 'Yor Forger';
