SELECT * FROM t_chat_role_setting where role_id = '40000008';

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.and.whisper', 'Android', '0.0.01', '9.9.99', '40000008', 'Spiderman',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman.jpg',
'[]', '[]',
1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.ios.whisperai', 'iOS', '0.0.01', '9.9.99', '40000008', 'Spiderman',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman.jpg',
'[]', '[]',
1, 1);


UPDATE t_chat_role SET interests = '["20000005"]' WHERE role_id like '40%' AND role_name = 'Spiderman';
UPDATE t_chat_role SET gender = 1 WHERE role_id like '40%' AND role_name = 'Spiderman';
UPDATE t_chat_role SET role_avatars = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_1.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_2.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_3.jpg"]'
WHERE role_id like '40%' AND role_name = 'Spiderman';

UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_1.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_2.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_3.mp4"]'
WHERE role_id like '40%' AND role_name = 'Spiderman';
UPDATE t_chat_role_setting SET role_animations = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_1.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_2.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Spiderman_3.mp4"]'
WHERE role_id like '40%' AND role_name = 'Spiderman';
