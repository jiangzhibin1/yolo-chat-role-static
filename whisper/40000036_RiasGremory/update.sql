SELECT * FROM t_chat_role_setting where role_id = '40000036';

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.and.whisper', 'Android', '0.0.01', '9.9.99', '40000036', 'Rias Gremory',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory.jpg',
'[]', '[]',
1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.ios.whisperai', 'iOS', '0.0.01', '9.9.99', '40000036', 'Rias Gremory',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory.jpg',
'[]', '[]',
1, 1);


UPDATE t_chat_role SET interests = '["20000006"]' WHERE role_id like '40%' AND role_name = 'Rias Gremory';
UPDATE t_chat_role SET gender = 2 WHERE role_id like '40%' AND role_name = 'Rias Gremory';
UPDATE t_chat_role SET role_avatars = '["https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory_1.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory_2.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory_3.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/RiasGremory_4.jpg"]'
WHERE role_id like '40%' AND role_name = 'Rias Gremory';

update t_chat_role set is_nsfw = 0 where role_id in ('40000038', '40000037', '40000036', '40000035', '40000034', '40000033');
update t_chat_role set is_new = 1 where role_id in ('40000041', '40000042', '40000043');


UPDATE t_chat_role_setting SET role_animations = '["https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/RiasGremory_1.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/RiasGremory_2.mp4"]'
WHERE role_id like '40%' AND role_name = 'Rias Gremory';