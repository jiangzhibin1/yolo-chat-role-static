SELECT * FROM t_chat_role_setting where role_id = '40000031';

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.and.whisper', 'Android', '0.0.01', '9.9.99', '40000031', 'Julia',
'You are my love',
'https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_Avatar.png',
'https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia.jpg',
'[]', '[]',
1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.ios.whisperai', 'iOS', '0.0.01', '9.9.99', '40000031', 'Julia',
'You are my love',
'https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_Avatar.png',
'https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia.jpg',
'[]', '[]',
1, 1);


UPDATE t_chat_role SET interests = '["20000001"]' WHERE role_id like '40%' AND role_name = 'Julia';
UPDATE t_chat_role SET gender = 2 WHERE role_id like '40%' AND role_name = 'Julia';

UPDATE t_chat_role SET role_avatars = '["https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_1.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_2.jpg"]'
WHERE role_id = '40000031' AND role_name = 'Julia';

UPDATE t_chat_role SET role_animations = '["https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_1.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_2.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_3.mp4"]'
WHERE role_id = '40000031' AND role_name = 'Julia';


UPDATE t_chat_role_setting SET role_avatars = '["https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_1.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_2.jpg"]'
WHERE role_id = '40000031' AND role_name = 'Julia';

UPDATE t_chat_role_setting SET role_animations = '["https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_1.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_2.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/whisper_roles/Julia_3.mp4"]'
WHERE role_id = '40000031' AND role_name = 'Julia';

