SELECT * FROM t_chat_role_setting where role_id = '40000041';

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.and.whisper', 'Android', '0.0.01', '9.9.99', '40000041', 'Olivia',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia.jpg',
'[]', '[]',
1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_system,
role_icon, role_image, role_avatars, role_animations, weight, status)
VALUES ('com.bytejourney.ios.whisperai', 'iOS', '0.0.01', '9.9.99', '40000041', 'Olivia',
'You are my love',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_Avatar.png',
'https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia.jpg',
'[]', '[]',
1, 1);


UPDATE t_chat_role SET interests = '["20000001"]' WHERE role_id like '40%' AND role_name = 'Olivia';
UPDATE t_chat_role SET gender = 2 WHERE role_id like '40%' AND role_name = 'Olivia';
UPDATE t_chat_role SET role_avatars = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_1.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_2.jpg", "https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_3.jpg"]'
WHERE role_id like '40%' AND role_name = 'Olivia';

UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_1.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_2.mp4", "https://storage.googleapis.com/yolo_chat/whisper_settings/Olivia_3.mp4"]'
WHERE role_id like '40%' AND role_name = 'Olivia';

创建目录
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/00_brain_lab/code -m "创建code目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/00_brain_lab/plan -m "创建plan目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/00_brain_lab/assets -m "创建assets目录"

svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/01_hidden_object_journey/code -m "创建code目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/01_hidden_object_journey/plan -m "创建plan目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/01_hidden_object_journey/assets -m "创建assets目录"

svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/02_defense_clash/code -m "创建code目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/02_defense_clash/plan -m "创建plan目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/02_defense_clash/assets -m "创建assets目录"

svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/03_war_survival/code -m "创建code目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/03_war_survival/plan -m "创建plan目录"
svn --username yladmin --password sDhLGyV8 mkdir svn://1.14.143.58/03_war_survival/assets -m "创建assets目录"

[00_brain_lab:/]
@yolo = rw

[00_brain_lab:/plan]
@yolo = rw
@plan = rw
@art = r
@code = r
@hewenhong = rw
@zhouping = rw
@liuwei = r
@yaozhu = r

[00_brain_lab:/code]
@yolo = rw
@code = rw
@plan = r
@liuwei = rw
@yaozhu = rw
@hewenhong = r
@zhouping = r

[00_brain_lab:/assets]
@yolo = rw
@art = rw
@miyo_asset = rw
@plan = r
@code = r
@liuwei = r
@yaozhu = r
@hewenhong = r
@zhouping = r


[01_hidden_object_journey:/]
@yolo = rw

[01_hidden_object_journey:/plan]
@yolo = rw
@plan = rw
@art = r
@code = r
@hewenhong = rw
@zhouping = rw
@liuwei = r
@yaozhu = r

[01_hidden_object_journey:/code]
@yolo = rw
@code = rw
@plan = r
@liuwei = rw
@yaozhu = rw
@hewenhong = r
@zhouping = r

[01_hidden_object_journey:/assets]
@yolo = rw
@art = rw
@miyo_asset = rw
@plan = r
@code = r
@liuwei = r
@yaozhu = r
@hewenhong = r
@zhouping = r


[02_defense_clash:/]
@yolo = rw

[02_defense_clash:/plan]
@yolo = rw
@plan = rw
@art = r
@code = r
@hewenhong = rw
@zhouping = rw
@liuwei = r
@yaozhu = r

[02_defense_clash:/code]
@yolo = rw
@code = rw
@plan = r
@liuwei = rw
@yaozhu = rw
@hewenhong = r
@zhouping = r

[02_defense_clash:/assets]
@yolo = rw
@art = rw
@miyo_asset = rw
@plan = r
@code = r
@liuwei = r
@yaozhu = r
@hewenhong = r
@zhouping = r


[03_war_survival:/]
@yolo = rw

[03_war_survival:/plan]
@yolo = rw
@plan = rw
@art = r
@code = r
@hewenhong = rw
@zhouping = rw
@liuwei = r
@yaozhu = r

[03_war_survival:/code]
@yolo = rw
@code = rw
@plan = r
@liuwei = rw
@yaozhu = rw
@hewenhong = r
@zhouping = r

[03_war_survival:/assets]
@yolo = rw
@art = rw
@miyo_asset = rw
@plan = r
@code = r
@liuwei = r
@yaozhu = r
@hewenhong = r
@zhouping = r


[general]
anon-access = none
auth-access = write
password-db = /data/svn/conf/passwd
authz-db = authz
groups-db = /data/svn/conf/groups

[match3:/plan]
@yolo = rw
@plan = rw
@art = r
@code = r

[match3:/code]
@yolo = rw
@code = rw
@plan = r

[match3-2:/plan]
@yolo = rw
@plan = rw
@art = r
@code = r

[match3-2:/code]
@yolo = rw
@code = rw
@plan = r
