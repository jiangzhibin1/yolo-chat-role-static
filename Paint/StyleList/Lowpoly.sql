delete from t_artwork_style where style_id = '100000012';

-- Low Poly
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000012', 'Low Poly',
'https://storage.googleapis.com/yolo_chat/artwork_settings/LowPoly.jpg',
'low ploy <lora:low:1>,',
'(worst quality:2),(low quality:2),(normal quality:2),lowres,bad anatomy,bad hands,normal quality,((monochrome)),((grayscale)) watermark,ng_deepnegative_v1_75t,bad_pictures,negative_hand-neg,easynegative,verybadimagenegative_v1.3,bad-hands-5,(:1.3),(badhandv4:1.2)',
'majicmixRealistic_betterV2V25.safetensors [d7e2ac2f4a]',
'DPM++ SDE Karras', 2, 30, 8, 88, 1);

-- wget "https://civitai.com/api/download/models/119059?type=Model&format=SafeTensor" -O low.safetensors