delete from t_artwork_style where style_id = '100000002';

-- Pill
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000002', 'Pill',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Pill.jpg',
'<lyco:p1ld31:1.2> p1ld3 style,',
'nsfw, low quality, letterboxed, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'dreamshaper_5BakedVae.safetensors [a60cfaa90d]',
'DPM++ SDE Karras', 2, 30, 8, 98, 1);

update t_artwork_style set positive_prompt_tpl = '(masterpiece, best quality),(HIGH RES, best quality:1.4)' where style_id = '100000002';
update t_artwork_style set weight = 100 where style_id = '100000000';

-- lycoris
wget "https://civitai.com/api/download/models/114162?type=Model&format=SafeTensor" -O p1ld31.safetensors
