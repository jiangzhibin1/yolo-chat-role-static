delete from t_artwork_style where style_id = '100000008';

-- Animal
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000008', 'Animal',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Animal.jpg',
'<lyco:GPTS3 animals_258424:1.0>, masterpiece, best quality, realistic, 8K, colorful, photorealistic, HDR, high detail, ',
'(worst quality, low quality:1.4),bad-picture-chill-75v, easynegative, ng_deepnegative_v1_75t, verybadimagenegative_v1.1-6400, (worst quality, low quality:1.4)',
'ghostmix_v12Bakedvae.safetensors [5280cb5146]',
'DPM++ SDE Karras', 2, 30, 8, 92, 1);

-- wget "https://civitai.com/api/download/models/64503?type=Model&format=SafeTensor&size=pruned&fp=fp32" -O ghostmix_v12Bakedvae.safetensors
-- wget "https://civitai.com/api/download/models/65352?type=Model&format=SafeTensor" -O "GPTS3 animals_258424.safetensors"
