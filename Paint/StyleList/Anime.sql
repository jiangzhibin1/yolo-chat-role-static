delete from t_artwork_style where style_id = '100000004';

-- Anime
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000004', 'Anime',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Anime.jpg',
'(masterpiece, best quality), ((intricate, print)),(HIGH RES, best quality:1.4),',
'(worst quality, low quality, letterboxed)',
'meinamix_meinaV11.safetensors [54ef3e3610]',
'DPM++ SDE Karras', 2, 30, 8, 96, 1);


-- wget "https://civitai.com/api/download/models/118094?type=Model&format=SafeTensor" -O "Colorful portraits_20230715165729-000018.safetensors"