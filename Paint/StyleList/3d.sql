delete from t_artwork_style where style_id = '100000009';

-- 3d
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000009', '3d',
'https://storage.googleapis.com/yolo_chat/artwork_settings/3d.jpg',
'chibi,epic realistic, faded, ((neutral colors)), art, (hdr:1.5), (muted colors:1.2), hyperdetailed, (artstation:1.5), cinematic, warm lights, dramatic light, (intricate details:1.1), (natural skin texture, hyperrealism, soft light, sharp:1.2), (intricate details:1.12), hdr, (intricate details, hyperdetailed:1.15), ',
'BadDream, UnrealisticDream FastNegativeV2, (UnrealisticDream:1.3)',
'3dAnimationDiffusion_v10.safetensors [31829c378d]',
'DPM++ SDE Karras', 2, 30, 8, 91, 1);

UPDATE t_artwork_style SET model_name = '3dAnimationDiffusion_v10.safetensors [31829c378d]' where style_id = '100000009';

-- wget "https://civitai.com/api/download/models/128046?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O 3dAnimationDiffusion_v10.safetensors
