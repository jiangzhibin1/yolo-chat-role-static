delete from t_artwork_style where style_id = '100000011';

-- Abstract
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000011', 'Abstract',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Abstract.jpg',
'in the style of t3xtn, <lora:t3xtn:0.45>, highly detailed, 8k, micro detail,',
'cartoon, 3d, ((disfigured)), ((bad art)), ((deformed)), ((poorly drawn)), ((extra limbs)), blurry, (Watermark:1.5),(Text:1.3),watermark,signature, monochrome, (worst quality, low quality:1.5), drawing,  blurry, (((duplicate))), ((morbid)), ((mutilated)), [out of frame], extra fingers, mutated hands, ((poorly drawn hands)), ((poorly drawn face)), (((mutation))), (((deformed))), ((ugly)), blurry, ((bad anatomy)), (((bad proportions))), ((extra limbs)), cloned face, (((disfigured)))',
'vinteprotogenmix_V20.safetensors [f2f29d2617]',
'DPM++ SDE Karras', 2, 30, 8, 89, 1);

-- wget "https://civitai.com/api/download/models/23690?type=Model&format=SafeTensor&size=full&fp=fp16" -O vinteprotogenmix_V20.safetensors
-- wget "https://civitai.com/api/download/models/26792?type=Model&format=SafeTensor&size=full&fp=fp16" -O t3xtn.safetensors
-- wget "https://civitai.com/api/download/models/94303?type=Model&format=SafeTensor" -O ElementalMagicAIv2-000008.safetensors
