delete from t_artwork_style where style_id = '100000000';

-- Comic
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000000', 'Comic',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Comic.jpg',
'(masterpiece, best quality), ((intricate, print)),(HIGH RES, best quality:1.4)',
'nsfw, low quality, letterboxed, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'toonyou_beta6.safetensors [e8d456c42e]', 'DPM++ SDE Karras', 2, 30, 8, 100, 1);

update t_artwork_style set positive_prompt_tpl = '(masterpiece, best quality),(HIGH RES, best quality:1.4)' where style_id = '100000000';
update t_artwork_style set weight = 100 where style_id = '100000000';
