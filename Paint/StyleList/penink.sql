delete from t_artwork_style where style_id = '100000007';

-- Pen Ink
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000007', 'Pen & Ink',
'https://storage.googleapis.com/yolo_chat/artwork_settings/PenInk.jpg',
'<lora:pensketch_lora_v2.3:0.8> penSketch_style, monochrome, ink sketch, masterpiece, best quality,',
'badhandv4, EasyNegative, signature, watermark, english text,',
'AnythingV5Ink_ink.safetensors [a1535d0a42]',
'DPM++ SDE Karras', 2, 30, 8, 93, 1);

-- wget "https://civitai.com/api/download/models/90854?type=Model&format=SafeTensor&size=full&fp=fp16" -O AnythingV5Ink_ink.safetensors
-- wget "https://civitai.com/api/download/models/35516?type=Model&format=SafeTensor" -O pensketch_lora_v2.3.safetensors
