delete from t_artwork_style where style_id = '100000010';

-- Magazine
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000010', 'Magazine',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Magazine.jpg',
'chibi,epic realistic, faded, ((neutral colors)), art, (hdr:1.5), (muted colors:1.2), hyperdetailed, (artstation:1.5), cinematic, warm lights, dramatic light, (intricate details:1.1), (natural skin texture, hyperrealism, soft light, sharp:1.2), (intricate details:1.12), hdr, (intricate details, hyperdetailed:1.15), ',
'BadDream, UnrealisticDream FastNegativeV2, (UnrealisticDream:1.3)',
'primemix_v21.safetensors [b79a4f7283]',
'DPM++ SDE Karras', 2, 30, 8, 90, 1);

update t_artwork_style set model_name = 'magazineprimemix_v21.safetensors [b79a4f7283]' where style_id = = '100000010';

-- wget "https://civitai.com/api/download/models/67388?type=Model&format=SafeTensor&size=full&fp=fp32" -O primemix_v21.safetensors
