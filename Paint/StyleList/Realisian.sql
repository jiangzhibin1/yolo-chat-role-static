delete from t_artwork_style where style_id = '100000006';

-- Realisian
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000006', 'Realisian',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Realisian.jpg',
'best quality, masterpiece, (realistic skin_details, sharp focus, masterpiece,best quality,finely detailed,extremely detailed, realistic detail,clear_image,realistic, high_resolution,distinct_image)',
'extra hand, extra foot, close up, (multiple view:1.4), missing finger, extra fingers, fused fingers, bad composition,cropped,crossed eyes, (breast out, head out of frame, nsfw, worst quality,bad quality:1.2),nipples, monochrome, greyscale, text, signature, watermark, mutated,',
'majicmixRealistic_betterV2V25.safetensors [d7e2ac2f4a]',
'DPM++ SDE Karras', 2, 30, 8, 94, 1);
