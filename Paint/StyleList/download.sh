# common
# model
wget "https://civitai.com/api/download/models/115942?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O realisian_v50.safetensors
wget "https://civitai.com/api/download/models/119057?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O meinamix_meinaV11.safetensors
wget "https://civitai.com/api/download/models/26792?type=Model&format=SafeTensor&size=full&fp=fp16" -O t3xtn.safetensors
wget "https://civitai.com/api/download/models/11745?type=VAE&format=Other" -O Chilloutmix.safetensors
wget "https://civitai.com/api/download/models/93208" -O darkSushiMixMix_225D.safetensors
wget "https://civitai.com/api/download/models/94303?type=Model&format=SafeTensor" -O ElementalMagicAIv2-000008.safetensors
wget "https://civitai.com/api/download/models/107366?type=Model&format=SafeTensor" -O 3DMM_V12.safetensors
wget "https://civitai.com/api/download/models/65018?type=Model&format=SafeTensor" -O kVoidEnergy-000001.safetensors
wget "https://civitai.com/api/download/models/64503?type=Model&format=SafeTensor&size=pruned&fp=fp32" -O ghostmix_v12Bakedvae.safetensors
wget "https://civitai.com/api/download/models/128713?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O dreamshaper_8.safetensors
wget "https://civitai.com/api/download/models/90854?type=Model&format=SafeTensor&size=full&fp=fp16" -O AnythingV5Ink_ink.safetensors
wget "https://civitai.com/api/download/models/46846?type=Model&format=SafeTensor&size=full&fp=fp32" -O revAnimated_v122.safetensors
wget "https://civitai.com/api/download/models/102?type=Model&format=PickleTensor&size=full&fp=fp16" -O vanGoghDiffusion_v1.ckpt
wget "https://civitai.com/api/download/models/21580?type=Model&format=SafeTensor&size=full&fp=fp16" -O "van Gogh.safetensors"
wget "https://civitai.com/api/download/models/74821?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O impressionismOil_sd15.safetensors
wget "https://civitai.com/api/download/models/23690?type=Model&format=PickleTensor&size=full&fp=fp16" -O vinteprotogenmix_V20.safetensors
wget "https://civitai.com/api/download/models/128046?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O 3dAnimationDiffusion_v10.safetensors
wget "https://civitai.com/api/download/models/80409?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O disneyPixarCartoon_v10.safetensors
wget "https://civitai.com/api/download/models/126470?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O majicmixRealistic_betterV2V25.safetensors
wget "https://civitai.com/api/download/models/67388?type=Model&format=SafeTensor&size=full&fp=fp32" -O primemix_v21.safetensors

wget "https://cdn-lfs.huggingface.co/repos/8d/3d/8d3df7f6affc35bfd2244dac894ed0c1204e489bbaa11caa47f6f40ae7b47ae8/a60cfaa90decb28a1447f955e29220eb867e8e157f0ca8a4d3b839cd9cd4832a?response-content-disposition=attachment%3B+filename*%3DUTF-8%27%27dreamshaper_5BakedVae.safetensors%3B+filename%3D%22dreamshaper_5BakedVae.safetensors%22%3B&Expires=1693470415&Policy=eyJTdGF0ZW1lbnQiOlt7IkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTY5MzQ3MDQxNX19LCJSZXNvdXJjZSI6Imh0dHBzOi8vY2RuLWxmcy5odWdnaW5nZmFjZS5jby9yZXBvcy84ZC8zZC84ZDNkZjdmNmFmZmMzNWJmZDIyNDRkYWM4OTRlZDBjMTIwNGU0ODliYmFhMTFjYWE0N2Y2ZjQwYWU3YjQ3YWU4L2E2MGNmYWE5MGRlY2IyOGExNDQ3Zjk1NWUyOTIyMGViODY3ZThlMTU3ZjBjYThhNGQzYjgzOWNkOWNkNDgzMmE%7EcmVzcG9uc2UtY29udGVudC1kaXNwb3NpdGlvbj0qIn1dfQ__&Signature=kdtkAXjPPiZFmpaFUYJKW4Q0xZV7FhLNIPAoKFLm%7EzW%7EwMPCZysXlPHtYHyMIUE4TAu8WzE0kQMxlX-RAqi7msmi5k38H71tS%7E8veXDzOhycHOH98HYqZ1879zJ8-BzN0QQKVjPseArS-bksg0OwWKjmVrV7CCvv8W5OPArrc6yl5H4f1mmQE5C2MIgEmmdD-EivfSOF80OtKmFrxlAP7JwYpvpFQGR08-cgrOptB13T2vjKu%7ERBT94KhSIBGjd2fzUXjy2ssHtwEynY%7ErCgLiNxUSCkdhvcZPIciLCkFl6skTi7JcjimDct2W0sDcRr0fYZSd6dB8dggBPgp%7EKsmg__&Key-Pair-Id=KVTP0A1DKRTAX" -O dreamshaper_5BakedVae.safetensors

# lora
wget "https://civitai.com/api/download/models/62833?type=Model&format=SafeTensor" -O add_detail.safetensors
wget "https://civitai.com/api/download/models/16576?type=Model&format=SafeTensor&size=full&fp=fp16" -O epi_noiseoffset2.safetensors
wget "https://civitai.com/api/download/models/83473?type=Model&format=SafeTensor" -O yautja-10.safetensors
wget "https://civitai.com/api/download/models/112969?type=Model&format=SafeTensor" -O FilmVelvia3.safetensors
wget "https://civitai.com/api/download/models/78583?type=Model&format=SafeTensor" -O pureInnocentGirl_v25.safetensors
wget "https://civitai.com/api/download/models/13244?type=Model&format=SafeTensor&size=full&fp=fp16" -O mikasa_ackerman_offset.safetensors
wget "https://civitai.com/api/download/models/26876?type=Model&format=SafeTensor&size=full&fp=fp16" -O suzumiya_haruhi_v10.safetensors
wget "https://civitai.com/api/download/models/87153?type=Model&format=SafeTensor" -O more_details.safetensors
wget "https://civitai.com/api/download/models/128601?type=Model&format=SafeTensor" -O "MengX girl_Mix_v30.safetensors"
wget "https://civitai.com/api/download/models/23250?type=Model&format=SafeTensor&size=full&fp=fp16" -O breastinclassBetter.safetensors
wget "https://civitai.com/api/download/models/55199?type=Model&format=SafeTensor" -O GoodHands-beta2.safetensors
wget "https://civitai.com/api/download/models/126681?type=Model&format=SafeTensor" -O Bellaria-01.safetensors
wget "https://civitai.com/api/download/models/126700?type=Model&format=SafeTensor" -O TamiFruit-01.safetensors
wget "https://civitai.com/api/download/models/52340?type=Model&format=SafeTensor" -O GirlfriendMix2.safetensors
wget "https://civitai.com/api/download/models/63006?type=Model&format=SafeTensor" -O LowRA.safetensors
wget "https://civitai.com/api/download/models/102546?type=Model&format=SafeTensor" -O amamiya_tsubaki_v1.safetensors
wget "https://civitai.com/api/download/models/19996?type=Model&format=SafeTensor&size=full&fp=fp16" -O HairCensorV1-000008.safetensors
wget "https://civitai.com/api/download/models/96428?type=Model&format=SafeTensor" -O mikasa_ackerman_v1.safetensors
wget "https://civitai.com/api/download/models/112867?type=Model&format=SafeTensor" -O edgHaute_Couturev3.safetensors
wget "https://civitai.com/api/download/models/59236?type=Model&format=SafeTensor" -O CHAR-SeceliaDote.safetensors
wget "https://civitai.com/api/download/models/13157?type=Model&format=SafeTensor&size=full&fp=fp16" -O Gojo.safetensors
wget "https://civitai.com/api/download/models/63245?type=Model&format=SafeTensor" -O gojo10epochs.safetensors
wget "https://civitai.com/api/download/models/116417?type=Model&format=SafeTensor" -O "动物模型丨柯基 MG_CORGI_V1.1.safetensors"
wget "https://civitai.com/api/download/models/50931?type=Model&format=SafeTensor" -O Ariel_character-20.safetensors
wget "https://civitai.com/api/download/models/116529?type=Model&format=SafeTensor" -O "chibi-v1.safetensors"
wget "https://civitai.com/api/download/models/14467?type=Model&format=SafeTensor&size=full&fp=fp16" -O "yuzusoft-chibiV2.safetensors"
wget "https://cdn-lfs.huggingface.co/repos/c5/ae/c5aeede072e27384e2e228ed9eae3cb8174f3456addc48be4ff610cf36dbc293/ba43b0efee61ace63eaa443aedc11432423a8398a9eec11e5ca33b9cec9a21d8?response-content-disposition=attachment%3B+filename*%3DUTF-8%27%27GoodHands-beta2.safetensors%3B+filename%3D%22GoodHands-beta2.safetensors%22%3B&Expires=1697785086&Policy=eyJTdGF0ZW1lbnQiOlt7IkNvbmRpdGlvbiI6eyJEYXRlTGVzc1RoYW4iOnsiQVdTOkVwb2NoVGltZSI6MTY5Nzc4NTA4Nn19LCJSZXNvdXJjZSI6Imh0dHBzOi8vY2RuLWxmcy5odWdnaW5nZmFjZS5jby9yZXBvcy9jNS9hZS9jNWFlZWRlMDcyZTI3Mzg0ZTJlMjI4ZWQ5ZWFlM2NiODE3NGYzNDU2YWRkYzQ4YmU0ZmY2MTBjZjM2ZGJjMjkzL2JhNDNiMGVmZWU2MWFjZTYzZWFhNDQzYWVkYzExNDMyNDIzYTgzOThhOWVlYzExZTVjYTMzYjljZWM5YTIxZDg%7EcmVzcG9uc2UtY29udGVudC1kaXNwb3NpdGlvbj0qIn1dfQ__&Signature=cF0YF7byieJoFD7kQdVAnhxtMkjymX3n3T5hSiA4EqWuD8ep%7ExOe-Jx3DajwVsbo9WcA6POqYuhdMPhWBSLer0A1T7nyJnDgoZDS3eiGYo-%7EiWk9ZdFPOVKg8awJ-KYkZse364mcWk1RN6f99WyzKx%7EnwJMNwintjRbdVqKP-dYJUteb4W72aoJAbCO6b8XHnBkE%7EyagGn168Zn3Lki0hRVjTtS-JWG1qPDY-LXieJdhaY64ay5PxNZCXMR3Y01J0u4RF7H82nzVfU4JXY-NcL-1p0boHwQ9CW2TQWyuWTPecXd8Ja-dWAdsXpkbKqlmRRL5FTc18jcACpQAQIYbXQ__&Key-Pair-Id=KVTP0A1DKRTAX" -O "GoodHands-beta2.safetensors"
wget "https://civitai.com/api/download/models/182369?type=Model&format=SafeTensor" -O "RyoSakazaki-DocStasis.safetensors"
wget "https://civitai.com/api/download/models/185865?type=Model&format=SafeTensor" -O "Ronald McDonald.safetensors"
wget "https://civitai.com/api/download/models/60868?type=Model&format=SafeTensor" -O "Halloween-07.safetensors"
wget "https://civitai.com/api/download/models/169813?type=Model&format=SafeTensor" -O "vintage.safetensors"
wget "https://civitai.com/api/download/models/94421?type=Model&format=SafeTensor" -O "Goth.safetensors"
wget "https://civitai.com/api/download/models/117467?type=Model&format=PickleTensor" -O "polyhedron_skinny_all.pt"
wget "https://civitai.com/api/download/models/94944?type=Model&format=SafeTensor" -O "Dreamwave v2L.safetensors"
wget "https://civitai.com/api/download/models/152309?type=Model&format=SafeTensor" -O "lfull_v1.safetensors"
wget "https://civitai.com/api/download/models/11905?type=Model&format=SafeTensor&size=full&fp=fp16" -O "lndcrtr_v1.0b.safetensors"
wget "https://civitai.com/api/download/models/60525?type=Model&format=SafeTensor" -O "santa_outfit.safetensors"

# Lora 迁移
scp -R sd@10.128.0.49:/data/stable-diffusion-webui/models/Lora/*.* ./

-- Animated Gif
wget "https://huggingface.co/guoyww/animatediff/resolve/main/mm_sd_v15.ckpt" -O "mm_sd_v15.ckpt"


# Lycoris
wget "https://civitai.com/api/download/models/55199?type=Model&format=SafeTensor" -O "GoodHands-beta2.safetensors"
wget "https://civitai.com/api/download/models/114162?type=Model&format=SafeTensor" -O p1ld31.safetensors

# Embeddings
wget "https://civitai.com/api/download/models/9208?type=Model&format=SafeTensor&size=full&fp=fp16" -O easynegative.safetensors
wget "https://civitai.com/api/download/models/6057?type=Negative&format=Other" -O "By bad artist -neg.pt"
wget "https://civitai.com/api/download/models/25820?type=Model&format=PickleTensor&size=full&fp=fp16" -O verybadimagenegative_v1.3.pt
wget "https://civitai.com/api/download/models/60095?type=Negative&format=Other" -O bad_prompt_version2-neg.pt
wget "https://civitai.com/api/download/models/5637?type=Model&format=PickleTensor&size=full&fp=fp16" -O ng_deepnegative_v1_75t.pt


# Abstract: https://civitai.com/posts/73926
wget "https://civitai.com/api/download/models/23690?type=Model&format=PickleTensor&size=full&fp=fp16" -O vinteprotogenmix_V20.safetensors
wget "https://civitai.com/api/download/models/26792?type=Model&format=SafeTensor&size=full&fp=fp16" -O t3xtn.safetensors
wget "https://civitai.com/api/download/models/94303?type=Model&format=SafeTensor" -O ElementalMagicAIv2-000008.safetensors
# https://huggingface.co/emmajoanne/models/blob/main/dreamshaper_5BakedVae.safetensors
wget "XXXXX" -O dreamshaper_5BakedVae.safetensors

# Comic: https://civitai.com/posts/431190
wget "https://civitai.com/api/download/models/125771?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O toonyou_beta6.safetensors

# Anime
wget "https://civitai.com/api/download/models/119057?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O meinamix_meinaV11.safetensors

# Dark: https://civitai.com/models/7371/rev-animated
wget "https://civitai.com/api/download/models/46846?type=Model&format=SafeTensor&size=full&fp=fp32" -O revAnimated_v122.safetensors
wget "https://civitai.com/api/download/models/65018?type=Model&format=SafeTensor" -O kVoidEnergy-000001.safetensors

# Disney
wget "https://civitai.com/api/download/models/80409?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O disneyPixarCartoon_v10.safetensors
wget "https://civitai.com/api/download/models/107366?type=Model&format=SafeTensor" -O 3DMM_V12.safetensors


# Dream Shaper
wget "https://civitai.com/api/download/models/128713?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O dreamshaper_8.safetensors

# Fantastic Animal
wget "https://civitai.com/api/download/models/64503?type=Model&format=SafeTensor&size=pruned&fp=fp32" -O ghostmix_v12Bakedvae.safetensors
wget "https://civitai.com/api/download/models/65352?type=Model&format=SafeTensor" -O "GPTS3 animals_258424.safetensors"
wget "https://civitai.com/api/download/models/64503?type=Model&format=SafeTensor&size=pruned&fp=fp32" -O ghostmix_v12Bakedvae.safetensors
wget "https://civitai.com/api/download/models/65352?type=Model&format=SafeTensor" -O "GPTS3 animals_258424.safetensors"
wget "https://civitai.com/api/download/models/64503?type=Model&format=SafeTensor&size=pruned&fp=fp32" -O ghostmix_v12Bakedvae.safetensors
wget "https://civitai.com/api/download/models/65352?type=Model&format=SafeTensor" -O "GPTS3 animals_258424.safetensors"

# Ghost Mix
wget "https://civitai.com/api/download/models/64503?type=Model&format=SafeTensor&size=pruned&fp=fp32" -O ghostmix_v12Bakedvae.safetensors


# Magazine
wget "https://civitai.com/api/download/models/67388?type=Model&format=SafeTensor&size=full&fp=fp32" -O primemix_v21.safetensors

# Novelistic
wget "https://civitai.com/api/download/models/79290?type=Model&format=SafeTensor&size=full&fp=fp16" -O aZovyaRPGArtistTools_v3.safetensors


# Oil Paint
wget "https://civitai.com/api/download/models/74821?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O impressionismOil_sd15.safetensors


# Pan & Ink
wget "https://civitai.com/api/download/models/90854?type=Model&format=SafeTensor&size=full&fp=fp16" -O AnythingV5Ink_ink.safetensors
wget "https://civitai.com/api/download/models/35516?type=Model&format=SafeTensor" -O pensketch_lora_v2.3.safetensors


# Realisian
wget "https://civitai.com/api/download/models/64094?type=Model&format=SafeTensor&size=full&fp=fp16" -O neverendingDreamNED_v122BakedVae.safetensors


# Science Fiction
wget "https://civitai.com/api/download/models/130121?type=Model&format=SafeTensor&size=full&fp=fp32" -O darksun_v41.safetensors


# Stories
wget "https://civitai.com/api/download/models/106092?type=Model&format=SafeTensor&size=full&fp=fp16" -O childrensStories_v1CustomA.safetensors

# Van Gogh
wget "https://civitai.com/api/download/models/102?type=Model&format=PickleTensor&size=full&fp=fp16" -O vanGoghDiffusion_v1.ckpt
wget "https://civitai.com/api/download/models/21580?type=Model&format=SafeTensor&size=full&fp=fp16" -O "van Gogh.safetensors"

# Vector Art
wget "https://civitai.com/api/download/models/68115?type=Model&format=SafeTensor" -O "0mib3(gut auf 1).safetensors"

