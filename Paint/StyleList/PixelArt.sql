delete from t_artwork_style where style_id = '100000015';

-- Pixel Art
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000015', 'Pixel Art',
'https://storage.googleapis.com/yolo_chat/artwork_settings/PixelArt.jpg',
'(masterpiece:1.2), (best quality), (ultra detailed), (8k, 4k, intricate),(full-body-shot:1), (highly detailed:1.2),(detailed face:1.2), (detailed background),detailed landscape, (dynamic pose:1.2), <lora:pixel:0.7>((pixelart))',
'(low quality:1.4), (worst quality:1.4), (monochrome:1.1),(bad_prompt_version2:0.8), (bad-hands-5:1.1), lowres,(Bored pose), static pose, busty bad hands, lowers, long body, disfigured, ugly, cross eyed, squinting, grain, Deformed, blurry, bad anatomy, poorly drawn face, mutation, mutated, extra limb, ugly, poorly drawn hands, missing limb, floating limbs, disconnected limbs, malformed hands, blur, out of focus, long neck, disgusting, poorly drawn, mutilated, ((text)), ((centered shot)), ((symetric pose)), ((symetric)), multiple views, multiple panels, blurry, multiple panels, blurry, watermark, letterbox, text, easynegative,',
'meinamix_meinaV11.safetensors [54ef3e3610]',
'DPM++ SDE Karras', 2, 30, 8, 85, 1);

-- wget "https://civitai.com/api/download/models/121559?type=Model&format=SafeTensor" -O pixelartV3.safetensors

