delete from t_artwork_style where style_id = '100000003';

-- Rainbow
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000003', 'Rainbow',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Rainbow.jpg',
'<lora:Colorful portraits_20230715165729-000018:0.9>,',
'nsfw, low quality, letterboxed, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'ghostmix_v12Bakedvae.safetensors [5280cb5146]', 'DPM++ SDE Karras', 2, 30, 8, 97, 1);


-- wget "https://civitai.com/api/download/models/118094?type=Model&format=SafeTensor" -O "Colorful portraits_20230715165729-000018.safetensors"