delete from t_artwork_style where style_id = '100000017';

-- Vintage
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000017', 'Vintage Ad',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Vintage.jpg',
'retro_artstyle, vintage ad style<lora:ad:1>',
'(worst quality:2),(low quality:2),(normal quality:2),lowres,bad anatomy,bad hands,normal quality,((monochrome)),((grayscale)) watermark,(nsfw:1.4),ng_deepnegative_v1_75t,bad_pictures,negative_hand-neg,easynegative,verybadimagenegative_v1.3,bad-hands-5, badhandv4',
'majicmixRealistic_betterV2V25.safetensors [d7e2ac2f4a]',
'DPM++ SDE Karras', 2, 30, 8, 83, 1);


-- wget "https://civitai.com/api/download/models/117773?type=Model&format=SafeTensor" -O ad.safetensors