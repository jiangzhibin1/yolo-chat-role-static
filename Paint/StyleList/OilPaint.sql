delete from t_artwork_style where style_id = '100000014';

-- OilPaint
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000014', 'Oil Paint',
'https://storage.googleapis.com/yolo_chat/artwork_settings/OilPaint.jpg',
'masterpiece, best quality,1girl, (fascist:1.15), (elemental:1.25), (lifeless:1.25), dreamwave <lora:Dreamwave sd11 lite10_69550:1>, BREAK, (masterpiece, best quality, absurdres:1.4), (highly detailed CG illustration), highly detailed, (extreme detailed:1.5),  <lora:add_detail:1>',
'(worst quality:2),(low quality:2),(normal quality:2),lowres,bad anatomy,bad hands,normal quality,((monochrome)),((grayscale)) watermark,ng_deepnegative_v1_75t,bad_pictures,negative_hand-neg,easynegative,verybadimagenegative_v1.3,bad-hands-5,(:1.3),(badhandv4:1.2)',
'impressionismOil_sd15.safetensors [a1535d0a42]',
'DPM++ SDE Karras', 2, 30, 8, 86, 1);

-- wget "https://civitai.com/api/download/models/74821?type=Model&format=SafeTensor&size=pruned&fp=fp16" -O impressionismOil_sd15.safetensors

