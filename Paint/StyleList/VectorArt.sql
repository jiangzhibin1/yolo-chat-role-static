delete from t_artwork_style where style_id = '100000016';

-- Pixel Art
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000016', 'Vector Art',
'https://storage.googleapis.com/yolo_chat/artwork_settings/VectorArt.jpg',
'((best quality)), ((masterpiece)), (detailed), vector <lora:0mib3(gut auf 1):0.6>',
'glass, pedestal, socle, basement, camera, subsurface, underwater, hypermaximalist, diamond, (flowers), water, (leaves), (woman:2), (1girl:2), human, (worst quality:2), (low quality:2), (normal quality:2), border, frame, poorly drawn, childish, hands, hand, ((dof))',
'revAnimated_v122.safetensors [4199bcdd14]',
'DPM++ SDE Karras', 2, 30, 8, 84, 1);

-- wget "https://civitai.com/api/download/models/46846?type=Model&format=SafeTensor&size=full&fp=fp32" -O revAnimated_v122.safetensors
-- wget "https://civitai.com/api/download/models/68115?type=Model&format=SafeTensor" -O "0mib3(gut auf 1).safetensors"

