delete from t_artwork_style where style_id = '100000000';




-- Disney

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, status)
VALUE ('100000001', 'Abstract',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Abstract.png',
'in the style of t3xtn, <lora:t3xtn:0.45>, highly detailed, 8k, micro detail',
'cartoon, 3d, ((disfigured)), ((bad art)), ((deformed)), ((poorly drawn)), ((extra limbs)), blurry, (Watermark:1.5),(Text:1.3),watermark,signature, monochrome, (worst quality, low quality:1.5), drawing,  blurry, (((duplicate))), ((morbid)), ((mutilated)), [out of frame], extra fingers, mutated hands, ((poorly drawn hands)), ((poorly drawn face)), (((mutation))), (((deformed))), ((ugly)), blurry, ((bad anatomy)), (((bad proportions))), ((extra limbs)), cloned face, (((disfigured)))',
'dreamshaper_5BakedVae.safetensors [a60cfaa90d]', 'DPM++ SDE Karras', 2, 30, 8, 1);

-- vinteprotogenmix_V20 报错: safetensors_rust.SafetensorError: Error while deserializing header: HeaderTooLarge
update t_artwork_style set model_name = 'dreamshaper_5BakedVae.safetensors [a60cfaa90d]' where style_id = '100000001';
update t_artwork_style set model_name = 'vinteprotogenmix_V20.safetensors [f2f29d2617]' where style_id = '100000001';


-- post: https://civitai.com/posts/431190
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, status)
VALUE ('100000002', 'Anime',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Anime.png',
'(masterpiece, best quality), ((intricate, print)),(HIGH RES, best quality:1.4)',
'nsfw, low quality, letterboxed, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'toonyou_beta6.safetensors [e8d456c42e]', 'DPM++ SDE Karras', 2, 30, 8, 1);

update t_artwork_style set positive_prompt_tpl = '(masterpiece, best quality),(HIGH RES, best quality:1.4)' where style_id = '100000002';
update t_artwork_style set weight = 99 where style_id = '100000002';

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, status)
VALUE ('100000003', 'Dark',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Dark.png',
'<lora:kVoidEnergy-000001:1>, V0id3nergy, <lora:style-kvoidenergy:0.6> masterpiece, best quality',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'revAnimated_v122.safetensors [4199bcdd14]', 'DPM++ SDE Karras', 2, 30, 8, 1);



delete from t_artwork_style where style_id = = '100000004';


INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, status)
VALUE ('100000005', 'Dream Shaper',
'https://storage.googleapis.com/yolo_chat/artwork_settings/DreamShaper.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'dreamshaper_5BakedVae.safetensors [a60cfaa90d]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, status)
VALUE ('100000006', 'Fantastic Animal',
'https://storage.googleapis.com/yolo_chat/artwork_settings/FantasticAnimals.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'ghostmix_v12Bakedvae.safetensors [5280cb5146]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000007', 'Ghost Mix',
'https://storage.googleapis.com/yolo_chat/artwork_settings/GhostMix.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'ghostmix_v12Bakedvae.safetensors [5280cb5146]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000008', 'Magazine',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Magazine.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'primemix_v21.safetensors [B79A4F7283]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000009', 'Novelistic',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Novelistic.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'aZovyaRPGArtistTools_v3.safetensors [C7751E6108]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000010', 'Oil Paint',
'https://storage.googleapis.com/yolo_chat/artwork_settings/OilPaint.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'impressionismOil_sd15.safetensors [A1535D0A42]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000011', 'Pan & Ink',
'https://storage.googleapis.com/yolo_chat/artwork_settings/PenInk.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'AnythingV5Ink_ink.safetensors [A1535D0A42]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000012', 'Realisian',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Realisian.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'neverendingDreamNED_v122BakedVae.safetensors [ECEFB796FF]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000013', 'Science Fiction',
'https://storage.googleapis.com/yolo_chat/artwork_settings/ScienceFiction.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'darksun_v41.safetensors [6D6E223662]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000014', 'Stories',
'https://storage.googleapis.com/yolo_chat/artwork_settings/Stories.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'childrensStories_v1CustomA.safetensors [2707AE64F2]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000015', 'Van Gogh',
'https://storage.googleapis.com/yolo_chat/artwork_settings/VanGogh.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'toonyou_beta6.safetensors [e8d456c42e]', 'DPM++ SDE Karras', 2, 30, 8, 1);

INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, status)
VALUE ('100000016', 'Vector Art',
'https://storage.googleapis.com/yolo_chat/artwork_settings/VectorArt.png',
'masterpiece, best quality, ',
'nsfw, lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'revAnimated_v122.safetensors [4199bcdd14]', 'DPM++ SDE Karras', 2, 30, 8, 1);

