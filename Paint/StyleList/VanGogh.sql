delete from t_artwork_style where style_id = '100000005';

-- Van Gogh
INSERT INTO t_artwork_style (style_id, style_name, thumbnail_url, positive_prompt_tpl, negative_prompt_tpl, model_name, sampler, clip_skip, steps, cfg_scale, weight, status)
VALUE ('100000005', 'Van Gogh',
'https://storage.googleapis.com/yolo_chat/artwork_settings/VanGosh.jpg',
'<lora:FG:1>,oil painting,Abstract painting',
'nsfw, low quality, letterboxed, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry, bad feet, ',
'vanGoghDiffusion_v1.ckpt [60b41cc82a]',
'DPM++ SDE Karras', 2, 30, 8, 95, 1);

-- wget "https://civitai.com/api/download/models/102?type=Model&format=PickleTensor&size=full&fp=fp16" -O vanGoghDiffusion_v1.ckpt
-- wget "https://civitai.com/api/download/models/21580?type=Model&format=SafeTensor&size=full&fp=fp16" -O "van Gogh.safetensors"