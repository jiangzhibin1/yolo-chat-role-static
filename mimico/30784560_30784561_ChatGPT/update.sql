insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.bot', 'Android', '1.5.60', '9.9.99', '30784560', 'Fast model', 'Talent AI', '',
'["I can provide information, answer questions, assist with writing and translation, and help with entertainment. Let me know what specific help you need."]',
'https://storage.googleapis.com/yolo_chat/role_avatar/chatgpt_3_5.png',
'[]',
'[]',
'Talk Now', '[]', 1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.base.bot', 'iOS', '1.5.60', '9.9.99', '30784561', 'Fast model ', 'Talent AI', '',
'["I can provide information, answer questions, assist with writing and translation, and help with entertainment. Let me know what specific help you need."]',
'https://storage.googleapis.com/yolo_chat/role_avatar/chatgpt_3_5.png',
'[]',
'[]',
'Talk Now', '[]', 1, 1);

UPDATE t_chat_role SET role_icon = 'https://storage.googleapis.com/yolo_chat/role_avatar/chatgpt_3_5.png' WHERE role_id like '30%' AND role_type = 'Talent AI';
UPDATE t_chat_role SET gender = -1 WHERE role_id like '30%' AND role_type = 'Talent AI';

select role_system from t_chat_role where role_id in ('30784560', '30784561');


UPDATE t_chat_role set role_system = 'I can provide information, answer questions, assist with writing and translation, and help with entertainment. Let me know what specific help you need.'
where role_id in ('30784560', '30784561');
