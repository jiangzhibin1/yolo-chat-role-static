SELECT * FROM t_chat_role_setting where role_id = '30784681';

insert into t_chat_role_setting (role_id, role_name, role_system, model_name, role_tags,
role_icon, role_image, role_avatars, role_animations, score, status, is_new, is_vip, is_nsfw, params)
VALUES ('30784681', 'Kate', 'You are my love', 'mythalion',
'["Beauty", "Fashion", "Dancing", "Pop Music"]',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_Avatar.png',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate.jpg',
'[]', '[]',
876, 1, 1, 0, 1, '{}');

insert into t_chat_pkg_role (role_id, pkg, start_cv, end_cv, weight, status)
VALUES ('30784681', 'com.elon.chat.bot', '0.0.01', '9.9.99', 1, 1);

insert into t_chat_pkg_role (role_id, pkg, start_cv, end_cv, weight, status)
VALUES ('30784681', 'com.elon.chat.base.bot', '0.0.01', '9.9.99', 1, 1);

UPDATE t_chat_role_setting SET interests = '["20000001"]' WHERE role_id = '30784681' AND role_name = 'Kate';
UPDATE t_chat_role_setting SET params = '{}' WHERE role_id = '30784681' AND role_name = 'Kate';
UPDATE t_chat_role_setting SET gender = 2 WHERE role_id = '30784681' AND role_name = 'Kate';
UPDATE t_chat_role_setting SET is_nsfw = 1 WHERE role_id = '30784681' AND role_name = 'Kate';

UPDATE t_chat_role_setting SET role_icon = 'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_avatar.png'
WHERE role_id = '30784681' AND role_name = 'Kate';

UPDATE t_chat_role_setting SET role_icon = 'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate.jpg'
WHERE role_id = '30784681' AND role_name = 'Kate';

UPDATE t_chat_role_setting SET role_avatars = '["https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_1.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_2.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_3.jpg"]'
WHERE role_id = '30784681' AND role_name = 'Kate';

UPDATE t_chat_role_setting SET role_animations = '["https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_1.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_2.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_3.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_4.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_5.mp4"]'
WHERE role_id = '30784681' AND role_name = 'Kate';

-- nsfw 21-23
INSERT INTO t_chat_role_media_asset (asset_id, role_id, category, url, preview_url, tags, greetings, weight, is_nsfw, status)
VALUES (
REPLACE(uuid(), '-', ''), '30784681', 'image',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_nude1.jpg',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_nude1_preview.jpg',
'[]', '[]', 1, 1, 1
);

INSERT INTO t_chat_role_media_asset (asset_id, role_id, category, url, preview_url, tags, greetings, weight, is_nsfw, status)
VALUES (
REPLACE(uuid(), '-', ''), '30784681', 'image',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_nude2.jpg',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_nude2_preview.jpg',
'[]', '[]', 1, 1, 1
);

INSERT INTO t_chat_role_media_asset (asset_id, role_id, category, url, preview_url, tags, greetings, weight, is_nsfw, status)
VALUES (
REPLACE(uuid(), '-', ''), '30784681', 'image',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_nude3.jpg',
'https://d2p3ztc5ul4j68.cloudfront.net/mimico_roles/Kate_nude3_preview.jpg',
'[]', '[]', 1, 1, 1
);

