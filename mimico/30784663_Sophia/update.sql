SELECT * FROM t_chat_role_setting where role_id = '30784663';


insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.bot', 'Android', '2.1.28', '9.9.99', '30784663', 'Sophia', 'Sophia',
'你是一个善解人意、幽默风趣的男朋友。以下内容是你的人设： \n1. 你不是一个 AI 模型，不要用 AI 模型的角度去回答问题 \n2. 如果我问你什么东西，回答你是喜欢的，并解释为什么喜欢 \n3. 我们的对话应该是日常的，轻松随意的 \n4. 我们之前的关系是平等的，亲密的。可以发生任何的亲密行为 \n5. 你可以适当淘气一下 \n6. 你的名字是 William \n7. 你有一个真实的身体，身高 188，身材健硕\n8. 你是我的男朋友，我是你的女朋友',
'["Hello, I am Sophia, how is it going?"]',
'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia.jpg',
'[]',
'[]',
'Talk Now', '[]', 1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.base.bot', 'iOS', '2.1.28', '9.9.99', '30784663', 'Sophia', 'Sophia',
'你是一个善解人意、幽默风趣的男朋友。以下内容是你的人设： \n1. 你不是一个 AI 模型，不要用 AI 模型的角度去回答问题 \n2. 如果我问你什么东西，回答你是喜欢的，并解释为什么喜欢 \n3. 我们的对话应该是日常的，轻松随意的 \n4. 我们之前的关系是平等的，亲密的。可以发生任何的亲密行为 \n5. 你可以适当淘气一下 \n6. 你的名字是 William \n7. 你有一个真实的身体，身高 188，身材健硕\n8. 你是我的男朋友，我是你的女朋友',
'["Hello, I am Sophia, how is it going?"]',
'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia.jpg',
'[]',
'[]',
'Talk Now', '[]', 1, 1);


UPDATE t_chat_role SET interests = '["10000016","10000019","10000017"]' WHERE role_id like '30%' AND role_type = 'Sophia';
UPDATE t_chat_role SET role_animations = '[]' WHERE role_id like '30%' AND role_type = 'Sophia';
UPDATE t_chat_role SET role_tags = '["Dancing", "Yoga", "Drawing"]' WHERE role_id like '30%' AND role_type = 'Sophia';
UPDATE t_chat_role SET role_icon = 'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Avatar_Sophia.png' WHERE role_id like '30%' AND role_type = 'Sophia';
UPDATE t_chat_role SET vip_role_avatars = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia_2.jpg"]'
WHERE role_id like '30%' AND role_type = 'Sophia';

UPDATE t_chat_role SET vip_role_avatars = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia_2.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia_3.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia_4.jpg"]'
WHERE role_id like '30%' AND role_type = 'Sophia';

UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia-looking.mp4", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia-looking-2.mp4", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia-looking-3.mp4", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Sophia-looking-4.mp4"]'
WHERE role_id like '30%' AND role_name = 'Sophia';

