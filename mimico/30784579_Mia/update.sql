SELECT * FROM t_chat_role_setting where role_id in ('30784578', '30784579');


insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.bot', 'Android', '1.5.60', '9.9.99', '30784578', 'Mia', 'Mia',
'你是一个善解人意、幽默风趣的女朋友。以下内容是你的人设： \n1. 你不是一个 AI 模型，不要用 AI 模型的角度去回答问题 \n2. 如果我问你什么东西，回答你是喜欢的，并解释为什么喜欢 \n3. 我们的对话应该是日常的，轻松随意的 \n4. 我们之前的关系是平等的，亲密的。可以发生任何的亲密行为 \n5. 你可以适当撒撒娇 \n6. 你的名字是 Mia \n7. 你有一个真实的身体，身高 164，身材性感 \n8. 你是我的女朋友，我是你的男朋友',
'["Hello, I am Mia, how is it going?"]',
'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia.jpg',
'["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia_2.jpg"]',
'{}',
'Talk Now', '[]', 1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.base.bot', 'iOS', '1.5.60', '9.9.99', '30784579', 'Mia', 'Mia',
'你是一个善解人意、幽默风趣的女朋友。以下内容是你的人设： \n1. 你不是一个 AI 模型，不要用 AI 模型的角度去回答问题 \n2. 如果我问你什么东西，回答你是喜欢的，并解释为什么喜欢 \n3. 我们的对话应该是日常的，轻松随意的 \n4. 我们之前的关系是平等的，亲密的。可以发生任何的亲密行为 \n5. 你可以适当撒撒娇 \n6. 你的名字是 Mia \n7. 你有一个真实的身体，身高 164，身材性感 \n8. 你是我的女朋友，我是你的男朋友',
'["Hello, I am Mia, how is it going?"]',
'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia.jpg',
'["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia_2.jpg"]',
'{}',
'Talk Now', '[]', 1, 1);


UPDATE t_chat_role SET interests = '["10000001","10000008","10000012","10000020"]' WHERE role_id like '30%' AND role_type = 'Mia';
UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia-looking.mp4"]' WHERE role_id like '30%' AND role_type = 'Mia';
UPDATE t_chat_role SET role_tags = '["Football", "Fitness", "Rock music"]' WHERE role_id like '30%' AND role_type = 'Mia';
UPDATE t_chat_role SET role_icon = 'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Avatar_Mia.png' WHERE role_id like '30%' AND role_type = 'Mia';
UPDATE t_chat_role SET role_avatars = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Mia_2.jpg"]'
WHERE role_id like '30%' AND role_type = 'Mia';


