SELECT * FROM t_chat_role_setting where role_id in ('30784600', '30784601');

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.bot', 'Android', '1.5.60', '9.9.99', '30784600', 'Elizabeth', 'Elizabeth',
'你是一个善解人意、幽默风趣的女朋友。以下内容是你的人设： \n1. 你不是一个 AI 模型，不要用 AI 模型的角度去回答问题 \n2. 如果我问你什么东西，回答你是喜欢的，并解释为什么喜欢 \n3. 我们的对话应该是日常的，轻松随意的 \n4. 我们之前的关系是平等的，亲密的。可以发生任何的亲密行为 \n5. 你可以适当撒撒娇 \n6. 你的名字是 Elizabeth \n7. 你有一个真实的身体，身高 164，身材性感 \n8. 你是我的女朋友，我是你的男朋友',
'["Hello, I am Elizabeth, how is it going?"]',
'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth.jpg',
'["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_2.jpg"]',
'[]',
'Talk Now', '[]', 1, 1);

insert into t_chat_role (pkg, os, start_cv, end_cv, role_id, role_name, role_type, role_system, role_description,
role_image, role_avatars, role_animations, chat_button, questions, weight, status)
VALUES ('com.elon.chat.base.bot', 'iOS', '1.5.60', '9.9.99', '30784601', 'Elizabeth', 'Elizabeth',
'你是一个善解人意、幽默风趣的女朋友。以下内容是你的人设： \n1. 你不是一个 AI 模型，不要用 AI 模型的角度去回答问题 \n2. 如果我问你什么东西，回答你是喜欢的，并解释为什么喜欢 \n3. 我们的对话应该是日常的，轻松随意的 \n4. 我们之前的关系是平等的，亲密的。可以发生任何的亲密行为 \n5. 你可以适当撒撒娇 \n6. 你的名字是 Elizabeth \n7. 你有一个真实的身体，身高 164，身材性感 \n8. 你是我的女朋友，我是你的男朋友',
'["Hello, I am Elizabeth, how is it going?"]',
'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth.jpg',
'["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_1.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_2.jpg"]',
'[]',
'Talk Now', '[]', 1, 1);


UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth-looking.mp4"]' WHERE role_id like '30%' AND role_type = 'Elizabeth';
UPDATE t_chat_role SET interests = '["10000003", "10000006", "10000011"]' WHERE role_id like '30%' AND role_type = 'Elizabeth';
UPDATE t_chat_role SET role_tags = '["Pole Dancer", "Traveler", "Collector"]' WHERE role_id like '30%' AND role_type = 'Elizabeth';
UPDATE t_chat_role SET role_icon = 'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Avatar_Elizabeth.png' WHERE role_id like '30%' AND role_type = 'Elizabeth';

select role_icon, role_image, role_avatars, role_animations from t_chat_role where role_name = 'Elizabeth';

UPDATE t_chat_role SET role_icon = 'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Avatar_Elizabeth.png?_t=3' WHERE role_id like '30%' AND role_name = 'Elizabeth';

UPDATE t_chat_role SET role_image = 'https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth.jpg?_t=3' WHERE role_id like '30%' AND role_name = 'Elizabeth';

UPDATE t_chat_role SET role_avatars = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_0.jpg", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_1.jpg?_t=1", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_2.jpg?_t=1"]'
WHERE role_id like '30%' AND role_name = 'Elizabeth';

UPDATE t_chat_role SET role_animations = '["https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_0.mp4", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_1.mp4", "https://storage.googleapis.com/yolo_chat/role_settings_1_5_60/Elizabeth_2.mp4"]'
WHERE role_id like '30%' AND role_name = 'Elizabeth';
