SELECT * FROM t_chat_role_setting where role_id = '50000030';

insert into t_chat_role_setting (role_id, role_name, role_system, model_name, role_tags,
role_icon, role_image, role_avatars, role_animations, score, status, is_new, is_vip, is_nsfw, params)
VALUES ('50000030', 'Natalie', 'You are my love', 'mythalion',
'["Beauty", "Fashion", "Dancing", "Pop Music"]',
'https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_avatar.png',
'https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie.jpg',
'[]', '[]',
876, 1, 1, 1, 0, '{}');

insert into t_chat_pkg_role (role_id, pkg, start_cv, end_cv, weight, status)
VALUES ('50000030', 'com.codejoy.hush.aos', '1.1.15', '9.9.99', 1, 1);

UPDATE t_chat_role_setting SET interests = '["20000001"]' WHERE role_id = '50000030' AND role_name = 'Natalie';
UPDATE t_chat_role_setting SET params = '{}' WHERE role_id = '50000030' AND role_name = 'Natalie';
UPDATE t_chat_role_setting SET gender = 2 WHERE role_id = '50000030' AND role_name = 'Natalie';
UPDATE t_chat_role_setting SET is_nsfw = 0 WHERE role_id = '50000030' AND role_name = 'Natalie';

UPDATE t_chat_role_setting SET role_icon = 'https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_avatar.png'
WHERE role_id = '50000030' AND role_name = 'Natalie';

UPDATE t_chat_role_setting SET role_image = 'https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie.jpg'
WHERE role_id = '50000030' AND role_name = 'Natalie';

UPDATE t_chat_role_setting SET role_avatars = '["https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_1.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_2.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_3.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_4.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_5.jpg", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_6.jpg"]'
WHERE role_id = '50000030' AND role_name = 'Natalie';

UPDATE t_chat_role_setting SET role_animations = '["https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_1.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_2.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_3.mp4", "https://d2p3ztc5ul4j68.cloudfront.net/hush_roles/Natalie_4.mp4"]'
WHERE role_id = '50000030' AND role_name = 'Natalie';

